import os
import pytest
from buildstream.testing import cli

DATA_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'project'
)

# Perfom a show to ensure that the plugin can be succesfully loaded
# and instantiated.
@pytest.mark.datafiles(DATA_DIR)
def test_instantiation(cli, datafiles):
    project = os.path.join(datafiles.dirname, datafiles.basename)
    result = cli.run(project=project, args=['show', 'basic-element.bst'])
    result.assert_success()
