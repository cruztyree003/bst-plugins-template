from buildstream import BuildElement

# A plugin which does nothing. This is used for illustrating the
# structure of a BuildStream plugin repository.
class BasicElement(BuildElement):
    pass

# Plugin entry point
def setup():
    return BasicElement
