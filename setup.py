#!/usr/bin/env python3
#
#  Copyright (C) 2019 Bloomberg LLP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Phil Dawson <phil.dawson@codethink.co.uk>

import sys

try:
    from setuptools import setup, find_packages
except ImportError:
    print("BuildStream requires setuptools in order to locate plugins. Install "
          "it using your package manager (usually python3-setuptools) or via "
          "pip (pip3 install setuptools).")
    sys.exit(1)

setup(
    name='bst-plugins-template',
    version="0.1.0",
    description="A template on which to base repositories containing BuildStream plugins.",
    license='LGPL',
    packages=find_packages(exclude=['tests', 'tests.*']),
    include_package_data=True,
    install_requires=[
        'requests',
        'setuptools'
    ],
    package_data={
        'buildstream': [
            'bst_plugins_template/elements/**.yaml'
        ]
    },
    entry_points={
        'buildstream.plugins': [
            'basic_element = bst_plugins_template.elements.basic_element',
        ]
    },
    extras_require={
        'test': [
            'pytest-datafiles',
            'pytest-env',
            'pytest-xdist',
            'pytest >= 3.1.0'
        ],
        'doc': [
            'sphinx',
            'sphinx-click',
            'sphinx_rtd_theme'
         ],
    },
    zip_safe=False
)  #eof setup()

