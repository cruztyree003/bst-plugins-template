BuildStream Plugins Template
****************************

This repo is designed to be used as a template on which to model a
repository containing plugins for 
`BuildStream <https://gitlab.com/BuildStream/buildstream>`_
